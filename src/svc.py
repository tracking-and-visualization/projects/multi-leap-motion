from gestureAnalysis import *
from sklearn.model_selection import RandomizedSearchCV\



def testSVC():
    model = svm.SVC()
    # testModel(clf, "..\\..\\12_01_19_#1")

    #Настройка параметров модели
    C = [0.7, 1.0, 1.2]
    kernel = ['linear', 'poly', 'rbf', 'sigmoid']
    coef0 = [-0.5, 0.0, 0.5]
    shrinking = [True, False]
    probability = [True, False]
    tol = [float(1e-4), float(5e-3), float(1e-2), float(1e-3)]
    max_iter = [100, 80, 150, 200]
    gamma = ['auto']

    hyperparameter_grid = {'C': C,
                           'kernel': kernel,
                           'coef0': coef0,
                           'shrinking': shrinking,
                           'probability': probability,
                           'tol': tol,
                           'max_iter': max_iter,
                           'gamma': gamma}


    # Set up the random search with 4-fold cross validation
    random_cv = RandomizedSearchCV(estimator=model,
        param_distributions=hyperparameter_grid,
        cv=5, n_iter=25,
        scoring='accuracy',
        n_jobs=-1, verbose=1,
        return_train_score=True,
        random_state=42)

    # folder = "..\\..\\12_01_19_#1"
    folder = "..\\..\\09022019"
    chooseBest(random_cv, folder, [1, 2, 3])
    chooseBest(random_cv, folder, [1, 2])
    chooseBest(random_cv, folder, [1])


testSVC()
