import os
import numpy as np
import json
import pandas as pd
from string import ascii_lowercase
from sklearn import datasets, svm, preprocessing, metrics
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from random import shuffle
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler

notNeededKeys = ["id", "Device"]

def makePathToFolderWithJson(parent, name):
    f = os.path.join(parent, name)
    return os.path.join(f, "OK")


def hasFile(dir, startWith):
    for f in os.listdir(dir):
        if f.startswith(startWith):
            return f
    return None


def processJson(pathToFile, withTarget=False, idsToProcess=None):
    f = open(pathToFile)
    basename = os.path.basename(pathToFile)
    nameInfo = basename.split("_")
    #id жеста
    target = int(nameInfo[1])
    needToSkip = False
    if idsToProcess:
        needToSkip = False if (target in idsToProcess) else True
    jsonString = f.read()
    resJson = json.loads(jsonString)
    data = []
    #d = dict()
    header = []

    for key in sorted(list(x for x in resJson.keys() if x not in notNeededKeys)):
        v = resJson[key]
        val = [v["x"], v["y"], v["z"]]
        header.append(key+'_x')
        header.append(key + '_y')
        header.append(key + '_z')
        if "w" in v:
            val.append(v["w"])
            header.append(key + '_w')
        data.extend(val)

    if withTarget:
        # data.append(ascii_lowercase[target])
        data.append(str(target))
        header.append("target")

    data = np.array(data)#, dtype='float64')


    return data, target, header, needToSkip


def getDataFromFolder(folder, cameras=(1, 2, 3), idsToProcess=None):
    #fullPathToFolder = os.path.join(os.getcwd(), folder)
    fullPathToFolder = os.path.abspath(folder)

    comp1Folder = makePathToFolderWithJson(fullPathToFolder, "comp1")
    comp2Folder = makePathToFolderWithJson(fullPathToFolder, "comp2")
    comp3Folder = makePathToFolderWithJson(fullPathToFolder, "comp3")

    jsonFiles = [f for f in os.listdir(comp1Folder) if os.path.isfile(os.path.join(comp1Folder, f)) and os.path.join(comp1Folder, f).endswith(".json")]
    res = []

    for jsonFile in jsonFiles:
        jsonFileStartWith = "_".join(jsonFile.split("_")[:4])
        data1, target1, header, needToSkip = processJson(os.path.join(comp1Folder, jsonFile), idsToProcess=idsToProcess)
        if needToSkip:
            continue

        f2 = hasFile(comp2Folder, jsonFileStartWith)
        if f2:
            data2, target2, _, _ = processJson(os.path.join(comp2Folder, f2))
        else:
            print(comp2Folder, jsonFileStartWith, " not found ")
            continue

        f3 = hasFile(comp3Folder, jsonFileStartWith)
        if f3:
            data3, target3, _, _ = processJson(os.path.join(comp3Folder, f3))
        else:
            print(comp3Folder, jsonFileStartWith, " not found ")
            continue

        data = []
        camerasData = {1: data1,
                       2: data2,
                       3: data3}
        for c in cameras:
            data.extend(camerasData[c])

        """
        data.extend(data1)
        if camerasNum > 1:
            data.extend(data2)
        if camerasNum > 2:
            data.extend(data3)
        """
        res.append((data, target1))

    headers = []
    for c in cameras:
        headers.extend([x + "_comp" + str(c) for x in header[:-1]])

    """
    headers.extend([x + "_comp1" for x in header[:-1]])
    if camerasNum > 1:
        headers.extend([x + "_comp2" for x in header[:-1]])
    if camerasNum > 2:
        headers.extend([x + "_comp3" for x in header[:-1]])
    """
    return res, headers

def makePandasDF(data, headers):
    table = [x[0] for x in data]
    df = pd.DataFrame(table, columns=headers)
    return df


#https://stackoverflow.com/questions/29294983/how-to-calculate-correlation-between-all-columns-and-remove-highly-correlated-on/43104383#43104383
def remove_collinear_features(x, threshold):
    '''
    Objective:
        Remove collinear features in a dataframe with a correlation coefficient
        greater than the threshold. Removing collinear features can help a model
        to generalize and improves the interpretability of the model.

    Inputs:
        threshold: any features with correlations greater than this value are removed

    Output:
        dataframe that contains only the non-highly-collinear features
    '''

    # Dont want to remove correlations between Energy Star Score
    dropColumns = []
    for i in range(1, 4):
        #6 типов жестов
        for j in range(7):
            colName = 'target_comp' + str(i) + '_' + str(j)
            if colName in x.columns:
                dropColumns.append(colName)
    x = x.drop(columns=dropColumns)

    # Calculate the correlation matrix
    corr_matrix = x.corr()
    iters = range(len(corr_matrix.columns) - 1)
    drop_cols = []

    # Iterate through the correlation matrix and compare correlations
    for i in iters:
        for j in range(i):
            item = corr_matrix.iloc[j:(j + 1), (i + 1):(i + 2)]
            col = item.columns
            row = item.index
            val = abs(item.values)

            # If correlation exceeds the threshold
            if val >= threshold:
                # Print the correlated features and the correlation value
                # print(col.values[0], "|", row.values[0], "|", round(val[0][0], 2))
                drop_cols.append(col.values[0])

    # Drop one of each pair of correlated columns
    drops = set(drop_cols)
    x = x.drop(columns=drops)

    # Add the score back in to the data
    #x['score'] = y

    return x

def cleanDF(df):
    # Copy the original data
    features = df.copy()

    # Select the categorical columns
    categorical_columns = ['target']
    Y = categorical_subset = df[categorical_columns]

    """
    # One hot encode
    categorical_subset = pd.get_dummies(categorical_subset)
    """

    # Select the numeric columns
    numeric_columns = [x for x in df.columns if x not in categorical_columns]

    # convert numeric columns
    X = df[numeric_columns] = df[numeric_columns].apply(pd.to_numeric)

    numeric_subset = df.select_dtypes('number')


    # Join the two dataframes using concat
    # Make sure to use axis = 1 to perform a column bind
    #features = pd.concat([numeric_subset, categorical_subset], axis=1)

    features = remove_collinear_features(numeric_subset, 0.5)
    features = pd.concat([features, categorical_subset], axis=1)
    #features.to_csv('tmp.csv')

    return features


def splitToTrainAndTest(X, Y):
    X, X_test, y, y_test = train_test_split(X, Y,
                                            test_size=0.3,
                                            random_state=42)
    return X, X_test, y, y_test


def scale(X, X_test):
    # Create the scaler object with a range of 0-1
    scaler = MinMaxScaler(feature_range=(0, 1))
    # Fit on the training data
    scaler.fit(X)
    # Transform both the training and testing data
    X = scaler.transform(X)
    X_test = scaler.transform(X_test)
    return X, X_test

#https://habr.com/ru/company/mlclass/blog/247751/
def prepareData(data, headers, splitToTrain=False):

    X = [x[0] for x in data]
    X = np.array(X, dtype=np.float64)
    Y = [x[1] for x in data]

    # normalize the data attributes
    #normalized_X = preprocessing.normalize(X)
    # standardize the data attributes
    #standardized_X = preprocessing.scale(X)


    #используем линейную регрессию для выбора лучших 7 атрибутов

    model = LogisticRegression(solver='liblinear', multi_class='auto')
    # create the RFE model and select 10 attributes
    rfe = RFE(model, 10)
    rfe = rfe.fit(X, Y)
    # summarize the selection of the attributes
    print("------------------------- ATTRIBUTES---------------------------------------------------")
    l = list(zip(headers, list(rfe.ranking_)))
    l.sort(key=lambda x: x[1])
    print(l[:20])
    """
    for i, v in enumerate(rfe.support_):
        if v:
            print(str(headers[i]) + ": " + str(rfe.ranking_[i]))
    """

    X = rfe.transform(X)

    if splitToTrain:
        X, X_test, y, y_test = splitToTrainAndTest(X, Y)
        X, X_test = scale(X, X_test)
        return X, X_test, y, y_test

    return X, Y

def testModel(model, folder):
    data, header = getDataFromFolder(folder)
    X, X_test, y, y_test = prepareData(data, header, True)
    print("-"*40)
    print(max(X[0]), max(X_test[0]))

    model.fit(X, y)

    # make predictions
    expected = y_test
    predicted = model.predict(X_test)

    accuracy = accuracy_score(y_test, predicted)
    print("Accuracy: %.2f%%" % (accuracy * 100.0))
    # summarize the fit of the model
    print(metrics.classification_report(expected, predicted))
    print(metrics.confusion_matrix(expected, predicted))


def chooseBest(random_cv, folder, cameras=(1, 2, 3), idsToProcess=None):
    #"..\\..\\randomData"
    data, header = getDataFromFolder(folder, cameras, idsToProcess)
    X, X_test, y, y_test = prepareData(data, header, True)

    # Fit on the training data
    random_cv.fit(X, y)

    # Find the best combination of settings
    print("Для камер: " + ','.join(map(str, cameras)))
    print(random_cv.best_estimator_)

    # make predictions
    expected = y_test
    predicted = random_cv.predict(X_test)

    accuracy = accuracy_score(y_test, predicted)
    print("Accuracy: %.2f%%" % (accuracy * 100.0))
    # summarize the fit of the model
    print(metrics.classification_report(expected, predicted))
    print(metrics.confusion_matrix(expected, predicted))
    return accuracy

if __name__ == "__main__":
    folder = "..\\..\\09022019"

    for i in range(1, 4):
        data, headers = getDataFromFolder(folder, i)
        print("For " + str(i) + " camera(s)")
        prepareData(data, headers)
        print()
        print()
