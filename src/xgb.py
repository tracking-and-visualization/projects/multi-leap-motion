#from xgboost.sklearn import XGBRegressor
import xgboost
from gestureAnalysis import *
from sklearn.model_selection import RandomizedSearchCV

model = xgboost.XGBClassifier(n_jobs=4)
#testModel(model, "..\\..\\12_01_19_#1")

#Настройка параметров модели
max_depth = [3, 2, 4, 5]
learning_rate = [0.1, 0.05, 0.2]
n_estimators = [100, 75, 150, 125]
booster = ['gbtree', 'gblinear', 'dart']
gamma = [0, 0.05, 0.01]
min_child_weight = [1, 2, 0]
max_delta_step = [0, 1]
subsample = [0.5, 1]
colsample_bytree = [0.5, 1]
colsample_bylevel = [0.5, 1]
reg_alpha  = [0, 0.5, 1]
reg_lambda = [0.5, 1]
scale_pos_weight = [0.5, 1]

hyperparameter_grid = {'max_depth': max_depth,
                       'learning_rate': learning_rate,
                       'n_estimators': n_estimators,
                       'booster': booster,
                       'gamma': gamma,
                       'min_child_weight': min_child_weight,
                       'max_delta_step': max_delta_step,
                       'subsample': subsample,
                       'colsample_bytree': colsample_bytree,
                       'colsample_bylevel': colsample_bylevel,
                       'reg_alpha': reg_alpha,
                       'reg_lambda': reg_lambda,
                       'scale_pos_weight': scale_pos_weight}


# Set up the random search with 4-fold cross validation
random_cv = RandomizedSearchCV(estimator=model,
    param_distributions=hyperparameter_grid,
    cv=5, n_iter=25,
    scoring = 'accuracy',
    n_jobs = -1, verbose = 1,
    return_train_score = True,
    random_state=42)

#folder = "..\\..\\12_01_19_#1"
#folder = "..\\..\\09022019"
folder = "..\\..\\rec_for_acc_100219"
ids = list(range(18, 21))
acc1 = chooseBest(random_cv, folder, [1, 2, 3], ids)
acc2 = chooseBest(random_cv, folder, [1, 2], ids)
acc3 = chooseBest(random_cv, folder, [1], ids)
print('='*40)
print(acc1, acc2, acc3)
