import matplotlib.pyplot as plt
x = [10, 16, 22, 28, 34, 40]
y = [0.9887, 0.9913, 0.9882, 0.9907, 1, 1]
plt.plot(x, y, 'bo', x, y, 'k')
plt.axis([9, 41, 0.985, 1.002])
plt.xlabel('Distance, cm')
plt.ylabel('Accuracy')
plt.show()
