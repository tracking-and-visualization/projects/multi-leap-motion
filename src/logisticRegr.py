from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import RandomizedSearchCV
from gestureAnalysis import *

model = LogisticRegression(random_state=42, n_jobs=-1)
#testModel(model, "..\\..\\randomData")

#Настройка параметров модели
tol = [float(1e-4), float(5e-4), float(1e-2), float(1e-3)]
C = [0.7, 1.0, 1.2]
fit_intercept = [True, False]
solver = ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga']
max_iter = [100, 80, 150, 200]

hyperparameter_grid = {'tol': tol,
                       'C': C,
                       'fit_intercept': fit_intercept,
                       'solver': solver,
                       'max_iter': max_iter}

# Set up the random search with 5-fold cross validation
random_cv = RandomizedSearchCV(estimator=model,
    param_distributions=hyperparameter_grid,
    cv=5, n_iter=25,
    scoring='accuracy',
    n_jobs=-1, verbose=1,
    return_train_score=True,
    random_state=42)

#folder = "..\\..\\12_01_19_#1"
folder = "..\\..\\09022019"
chooseBest(random_cv, folder, [1, 2, 3])
chooseBest(random_cv, folder, [1, 2])
chooseBest(random_cv, folder, [1])
